package com.piglite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/demo")
public class MyController2 {
    @RequestMapping("/welcome2")
    public String welcome(){
        return "welcome3";
    }

    @RequestMapping("/welcome4")
    public String welcome4(){
        return "welcome4";
    }
}
